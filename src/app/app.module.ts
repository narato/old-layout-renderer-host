import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import { COMPILER_PROVIDERS } from '@angular/compiler';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    COMPILER_PROVIDERS // this is an app singleton declaration
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

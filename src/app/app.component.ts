import {
  Compiler,
  Component,
  Injector,
  ModuleWithComponentFactories,
  NgModule,
  SystemJsNgModuleLoader,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { JitCompilerFactory } from '@angular/compiler';
import { Http, Response } from '@angular/http';

import { DynamicImportConfig } from './dynamic-import-config';

@Component({
  selector: 'app-root',
  template: `
    <h1>Layout renderer</h1>
    <button (click)="renderTemplateFromStore()">Render template</button>
    <div #container></div>
  `,
  styles: []
})
export class AppComponent {
  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  private moduleStore = 'http://localhost:3002/';
  private templateStore = 'http://localhost:3002/';
  private templateId = 'templates/my-template-1.html';
  private compiler: Compiler = new JitCompilerFactory([{useDebug: false, useJit: true}]).createCompiler();

  constructor(private injector: Injector, private http: Http) {}

  renderTemplateFromStore() {
    const templateUrl = this.templateStore + this.templateId;
    const dynamicImports = [];
    this.http.get(templateUrl).map( (response: Response) => response.text() ).subscribe( (html: string) => {
      const allMatches = html.match(/<!-- abs-import \'(.*)#(.*)\' -->/g) || [];
      allMatches.forEach((fullMatch) => {
        const matches = fullMatch.match(/<!-- abs-import \'(.*)#(.*)\' -->/);
        const dynamicImport = new DynamicImportConfig();
        dynamicImport.modulePath = matches[1]; // matches[0] is the complete string match
        dynamicImport.moduleName = matches[2];
        dynamicImports.push(dynamicImport);
      });

      this.loadImportedModules(html, dynamicImports);
    }, (error: any) => {
      console.log('Something went wrong');
    });

  }

  private loadImportedModules(html: string, dynamicImports: DynamicImportConfig[]) {
    const fileToLoad = dynamicImports.map( (config) => this.moduleStore + config.modulePath)[0];
    this.loadScript(fileToLoad, () => {
      const fileContent = window['sample'];
      const importedModules = dynamicImports.map( (config) => {
        const importedModule = NgModule(NgModule(fileContent[config.moduleName].decorators[0].args[0]))(fileContent[config.moduleName]);
        // compile dynamic module
        const modFactory = this.compiler.compileModuleAndAllComponentsSync(importedModule);
        // get moduleType of the dynamically loaded module
        return modFactory.ngModuleFactory.moduleType;
      });
      this.renderTemplate(html, importedModules);
    });
  }

  private loadScript(url: string, callback: any) {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.onload = function(){
        callback();
    };

    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  private renderTemplate(template: string, importedModules: any[]) {
    @Component({
      selector: 'abs-dynamic-root',
      template: template
    })
    class DynamicRootComponent {};

    @NgModule({
      imports: [importedModules[0].forRoot()],
      declarations: [ DynamicRootComponent ],
      exports: [ DynamicRootComponent ]
    })
    class DynamicRootModule {};

    const modFactory = this.compiler.compileModuleAndAllComponentsSync(DynamicRootModule);
    const cmpFactory = modFactory.componentFactories.find(f => f.componentType === DynamicRootComponent);

    this.container.createComponent(cmpFactory);
  }
}

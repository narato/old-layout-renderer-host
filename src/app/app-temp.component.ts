import {
  Compiler,
  Component,
  Injector,
  ModuleWithComponentFactories,
  NgModule,
  SystemJsNgModuleLoader,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { JitCompilerFactory } from '@angular/compiler';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { DynamicImportConfig } from './dynamic-import-config';

@Component({
  selector: 'app-root',
  template: `
    <h1>Layout renderer</h1>
    <button (click)="renderTemplateFromStore()">Render template</button>
    <div #container></div>
  `,
  styles: []
})
export class AppComponent {
  @ViewChild('container', {read: ViewContainerRef}) container: ViewContainerRef;

  private moduleStore = 'http://localhost:3002/';
  private templateStore = 'http://localhost:3002/';
  private templateId = 'templates/my-template-1.html';
  private compiler: Compiler = new JitCompilerFactory([{useDebug: false, useJit: true}]).createCompiler();

  constructor(private injector: Injector, private http: Http) {}

  renderTemplateFromStore() {
    const templateUrl = this.templateStore + this.templateId;
    const dynamicImports = [];
    this.http.get(templateUrl).map( (response: Response) => response.text() ).subscribe( (html: string) => {
      const allMatches = html.match(/<!-- abs-import \'(.*)#(.*)\' -->/g) || [];
      allMatches.forEach((fullMatch) => {
        const matches = fullMatch.match(/<!-- abs-import \'(.*)#(.*)\' -->/);
        const dynamicImport = new DynamicImportConfig();
        dynamicImport.modulePath = matches[1]; // matches[0] is the complete string match
        dynamicImport.moduleName = matches[2];
        dynamicImports.push(dynamicImport);
      });

      const importedModulesObservable = this.loadImportedModules(dynamicImports);
      this.renderTemplate(html, importedModulesObservable);
    }, (error: any) => {
      console.log('Something went wrong');
    });

  }

  private loadImportedModules(dynamicImports: DynamicImportConfig[]): Observable<any> {
    const observables = dynamicImports.map((config) => {
      const fileToLoad = this.moduleStore + config.modulePath;
      return this.loadScript(fileToLoad, () => {
        // todo: get the name of the module from a parameter
        const moduleContent = window['sample'];
        // build NgModule based on content of the remote umd module
        const importedModule = NgModule(NgModule(moduleContent[config.moduleName].decorators[0].args[0]))(moduleContent[config.moduleName]);
        // compile dynamic module
        const modFactory = this.compiler.compileModuleAndAllComponentsSync(importedModule);
        // get moduleType of the dynamically loaded module
        return modFactory.ngModuleFactory.moduleType;
      });
    });
    return Observable.zip(observables);
  }

  private loadScript(url: string, callback: any): Observable<any> {
    return Observable.create((observer) => {
      const script = document.createElement('script');
      script.type = 'text/javascript';
      // todo: Add IE support
      script.onload = function(){
        observer.onNext(callback());
        observer.onCompleted();
      };

      script.src = url;
      document.getElementsByTagName('head')[0].appendChild(script);
    });
  }

  private renderTemplate(template: string, importedModulesObservable: Observable<any>) {
    importedModulesObservable.subscribe((importedModules) => {
      @Component({
        selector: 'abs-dynamic-root',
        template: template
      })
      class DynamicRootComponent {};

      @NgModule({
        imports: importedModules.map(m => m.forRoot()),
        declarations: [ DynamicRootComponent ],
        exports: [ DynamicRootComponent ]
      })
      class DynamicRootModule {};

      const modFactory = this.compiler.compileModuleAndAllComponentsSync(DynamicRootModule);
      const cmpFactory = modFactory.componentFactories.find(f => f.componentType === DynamicRootComponent);

      this.container.createComponent(cmpFactory);
    });
  }
}

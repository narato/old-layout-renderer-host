export class DynamicImportConfig {
  componentName: string;
  moduleName: string;
  modulePath: string;
}
